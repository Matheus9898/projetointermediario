const fs = require('fs');

const ARQUIVO = "./api/data_old/evento.json";

function getEvento() {
    return JSON.parse(fs.readFileSync(ARQUIVO, "utf8"));
}

function saveEvento(evento){
    return fs.writeFileSync(ARQUIVO, JSON.stringify(evento), { encoding: "utf8", flag:"w"});
}

module.exports = {
    getEvento,
    saveEvento
};