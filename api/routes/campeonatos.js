const express = require("express");
const router = express.Router();
const Campeonato = require('../models/Campeonato')

// GET ALL Consulta Geral 
router.get("/", (req, res) => {   
    
    Campeonato.find((err, doc) => {
        if(!err) {
            res.json(doc)
        }
    })
});

// GET ONE Consulta Única 
router.get("/:id", (req, res) => {
    
    let parametro_id = req.params.id

    Campeonato.findOne({ _id: parametro_id}, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            console.error(err)
        }
    })
});

// POST Inserção 
router.post("/", (req, res) => {
    
    const conteudo = req.body;  
    var novaCampeonato = new Campeonato(conteudo);
    res.json(novaCampeonato)
    novaCampeonato.save();
});

// PUT Atualização 
router.put('/:id', (req, res) => {
    
    let parametro_id = req.params.id
    let updateCampeonato = req.body

    Campeonato.findOneAndUpdate({ _id: parametro_id }, updateCampeonato, (err, doc) => {
        if(!err) {
            res.json(updateCampeonato)
        } else{
            console.error(err)
        }
    })
    
});

// DELETE
router.delete('/:id', (req, res) => {

    let param_id = req.params.id
    Campeonato.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })

});

module.exports = router;