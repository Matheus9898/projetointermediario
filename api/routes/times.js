const express = require("express");
const router = express.Router();
const Time = require("../models/Time")

// GET ALL Consulta Geral 
router.get("/", (req, res) => {   
    Time.find((err, doc) => {
        if(!err) {
            res.json(doc)
        }
    })
});

// GET ONE Consulta Única 
router.get("/:id", (req, res) => {

    let parametro_id = req.params.id

    Time.findOne({ _id: parametro_id }, (err, doc) => {
        if(!err){
            res.json(doc)
        } else{
            console.error(err)
        }
    })
});

// POST Inserção 
router.post("/", (req, res) => {
    const conteudo = req.body
    var novoTime = new Time(conteudo)
    res.json(novoTime)
    novoTime.save()
});

// PUT Atualização 
router.put('/:id', (req, res) => {
    
    let parametro_id = req.params.id
    let updateTime = req.body

    Time.findOneAndUpdate({ _id: parametro_id }, updateTime, (err, doc) => {
        if(!err) {
            res.json(updateTime)
        } else{
            console.error(err)
        }
    })
});

// DELETE
router.delete('/:id', (req, res) => {

    let param_id = req.params.id
    Time.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })
});

module.exports = router;