const express = require("express");
const router = express.Router();
const Tecnico = require('../models/Tecnico')

// GET ALL Consulta Geral 
router.get("/", (req, res) => {   
    Tecnico.find((err, doc) => {
        if(!err) {
            res.json(doc)
        }
    })
});

// GET ONE Consulta Única 
router.get("/:id", (req, res) => {

    let parametro_id = req.params.id

    Tecnico.findOne({ _id: parametro_id}, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            console.error(err)
        }
    })
});

// POST Inserção 
router.post("/", (req, res) => {

    const conteudo = req.body;  
    var novoTecnico = new Tecnico(conteudo);
    res.json(novoTecnico)
    novoTecnico.save();
});

// PUT Atualização 
router.put('/:id', (req, res) => {
    
    let parametro_id = req.params.id
    let updateTecnico = req.body

    Tecnico.findOneAndUpdate({ _id: parametro_id }, updateTecnico, (err, doc) => {
        if(!err) {
            res.json(updateTecnico)
        } else{
            console.error(err)
        }
    })
});

// DELETE
router.delete('/:id', (req, res) => {
    
    let param_id = req.params.id
    Tecnico.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })
});

module.exports = router;