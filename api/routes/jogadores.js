const express = require("express");
const router = express.Router();
const Jogador = require('../models/Jogador')

// GET ALL Consulta Geral 
router.get("/", (req, res) => {   
    Jogador.find((err, doc) => {
        if(!err) {
            res.json(doc)
        } 
    })
});

// GET ONE Consulta Única 
router.get("/:id", (req, res) => {

    let parametro_id = req.params.id

    Jogador.findOne({ _id: parametro_id}, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            console.error(err)
        }
    })
});

// POST Inserção 
router.post("/", (req, res) => {
    const conteudo = req.body;
    var novoJogador = new Jogador(conteudo);
    res.json(novoJogador)
    novoJogador.save();
});

// PUT Atualização 
router.put('/:id', (req, res) => {

    let parametro_id = req.params.id
    let updateJogador = req.body

    Jogador.findOneAndUpdate({ _id: parametro_id }, updateJogador, (err, doc) => {
        if(!err) {
            res.json(updateJogador)
        } else{
            console.error(err)
        }
    })
});

// DELETE
router.delete('/:id', (req, res) => {
    
    let param_id = req.params.id
    Jogador.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })
});

module.exports = router;