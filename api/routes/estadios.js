const express = require("express");
const router = express.Router();
const Estadio = require('../models/Estadio')

// GET ALL Consulta Geral 
router.get("/", (req, res) => {   

    Estadio.find((err, doc) => {
        if(!err) {
            res.json(doc)
        }
    })
});

// GET ONE Consulta Única 
router.get("/:id", (req, res) => {

    let parametro_id = req.params.id

    Estadio.findOne({ _id: parametro_id}, (err, doc) => {
        if(!err){
            res.json(doc)
        } else {
            console.error(err)
        }
    })
});

// POST Inserção 
router.post("/", (req, res) => {

    const conteudo = req.body;  
    var novoEstadio = new Estadio(conteudo);
    res.json(novoEstadio)
    novoEstadio.save();
});

// PUT Atualização 
router.put('/:id', (req, res) => {
    let parametro_id = req.params.id
    let updateEstadio = req.body

    Estadio.findOneAndUpdate({ _id: parametro_id }, updateEstadio, (err, doc) => {
        if(!err) {
            res.json(updateEstadio)
        } else{
            console.error(err)
        }
    })
});

// DELETE 
router.delete('/:id', (req, res) => {
    
    let param_id = req.params.id
    Estadio.findByIdAndDelete(param_id, (err, doc) => {
        if(!err){
            res.json(doc)
        }
    })
});

module.exports = router;