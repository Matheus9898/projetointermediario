const mongoose = require('../data')

var jogadorScheme = new mongoose.Schema({
    'nome': String,
    'ano_nascimento': Number
})

var Jogador = mongoose.model('Jogador', jogadorScheme)

module.exports = Jogador