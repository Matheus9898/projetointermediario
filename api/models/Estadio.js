const mongoose = require('../data')

var estadioScheme = new mongoose.Schema({
    'nome': String,
    'capacidade': Number
})

var Estadio = mongoose.model('Estadio', estadioScheme)

module.exports = Estadio