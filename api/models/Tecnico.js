const mongoose = require('../data')

var tecnicoScheme = new mongoose.Schema({
    'nome': String,
    'nascionalidade': String
})

var Tecnico = mongoose.model('Tecnico', tecnicoScheme)

module.exports = Tecnico