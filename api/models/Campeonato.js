const mongoose = require('../data')

var campeonatoScheme = new mongoose.Schema({
    'nome': String,
    'local': String
})

var Campeonato = mongoose.model('Campeonato', campeonatoScheme)

module.exports = Campeonato