const express = require("express");
const app = express();
const rotaCampeonatos = require("./api/routes/campeonatos");
const rotaEstadios = require("./api/routes/estadios");
const rotaJogadores = require("./api/routes/jogadores");
const rotaTecnicos = require("./api/routes/tecnicos");
const rotaTimes = require("./api/routes/times");
const mongoose = require('./api/data');
const cors = require('cors')

app.use(express.json());
app.use(cors());

app.use("/campeonatos", rotaCampeonatos);
app.use("/estadios", rotaEstadios);
app.use("/jogadores", rotaJogadores);
app.use("/tecnicos", rotaTecnicos);
app.use("/times", rotaTimes);

mongoose.connection.on('connected', () =>{
    app.listen(8888, () => {
        console.log('Aplicação esta sendo executada na porta 8888')
    });
})