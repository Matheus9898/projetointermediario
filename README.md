Projeto Intermediario - JPW

A API tem como tema "Futebol" seus recursos são: Campeonato; Estadio; Jogador; Tecnico; Time.

* Para instalar as depências execute "npm install";
* Para executar a aplicação utilize "npm start";

A estrutura consiste em:

Para cadastrar um novo item, segue o exemplo abaixo:

Primeiro recurso: 
Campeonato{
        "_id": 1,
        "nome": "Libertadores",
        "local": "America do Sul"
    }

Segundo recurso:
Estadio{
        "_id": 1,
        "nome": "Estádio Beira-Rio",
        "capacidade": 55.128
    }

Terceiro recurso:
Jogador{
        "_id": 1,
        "nome": "Andrés Nicolás D'Alessandro",
        "ano_nascimento": 1981
    }

Quarto recurso:
Tecnico{
        "_id": 1,
        "nome": "Eduardo Germán Coudet",
        "nascionalidade": "Argentino"
    }

Quinto recurso:
Time{
        "_id": 1,
        "nome": "Sport Club Internacional",
        "fundacao": 1909
    }